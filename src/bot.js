const irc = require("irc");

var password = "foo";	//it's initialized with something

function main() {
	if(process.argv.length == 2) {
		console.error("Please add the nickserv password");
		process.exit(1);
	}
	password = process.argv[2];
}

main();

const bot = new irc.Client("irc.shelltalk.net", "donorbot", 
{
	userName: "donorBot",
	realName: "see robyn for details",
	port: 6667,
	channels: ["#robyn-test"],
	floodProtection: true
});

bot.on("error", (msg) => console.log(msg));

bot.on("mosr", (motd) =>{ bot.say("nickserv", "identify " + password); });

bot.addListener("pm", (nick, text, message) => {
	nick = nick.toLowerCase();
	text = text.toLowerCase();
	msg = text.split(" ");
	if(nick === "robyn") {
		if(msg[0] === "quit") {
			bot.disconnect();
			process.exit();
		}
		if(msg[0] === "leave") {
			bot.part(msg[1], "yes Boss");
		}
		if(msg[0] === "join") {
			bot.join(msg[1]);
		}
	}
});

bot.on("message#robyn-test", (nick, text, message) => {
	text = text.toLowerCase();
	text = text.split(' ');
	
	if(text[0] == "hello") {
			if(text[1] == "donorbot") {
			bot.say(message.args[0], `Hello ${nick}` );
		}
	}
});